package controller;


import java.io.Serializable;
import java.util.Date;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.Name;
import org.openntf.domino.Session;
import org.openntf.domino.utils.Factory;

import com.notesin9.tracker.Project;
import com.notesin9.tracker.Task;

public class project extends controller.base {

	
	private static final long serialVersionUID = 1L;
	private boolean valid;
	private Project currentProject;
	private Task currentTask;
	private boolean readOnly;

	
	public project() {
		// empty constructor
		this.readOnly = true;
	}
	
	public void pageInit() {
		
		if (base.getParam("unique").isEmpty()) {
			// No key.  Create a new Project
			this.print("Create Project");
			this.currentProject = new Project();
			this.currentProject.create();
			this.readOnly = false;
		
			
		} else {
			this.print("Key = " + base.getParam("unique"));
			this.currentProject = new Project();
			this.currentProject.load(base.getParam("unique"));
			this.print("Loaded");
		}
		
		this.valid = this.currentProject.isValid();		
		
		
	}

	public Project getCurrentProject() {
		return currentProject;
	}

	public void setCurrentProject(Project currentProject) {
		this.currentProject = currentProject;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	public void remove(String key) {
		this.print("Deleting Project");
		Project temp = new Project();
		temp.load(key);
		
		if (temp.isValid()) {
			temp.remove();
		}
		
		
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}
	
	public void newTask() {
		
		this.currentTask = this.currentProject.createTask();
		
		
		
	}

	public Task getCurrentTask() {
		return currentTask;
	}

	public void setCurrentTask(Task currentTask) {
		this.currentTask = currentTask;
	}
	
	public void openTask(Task myTask) {
		
		this.setCurrentTask(myTask);
		
	}
	
	public void taskSaveButton() {
		this.getCurrentTask().save();
		this.getCurrentProject().refreshTasks();
		
	}
	
	public void taskRemoveButton(Task rowData) {
		
		
		this.currentTask = new Task();  // might not be needed but can't hurt
		
		rowData.remove();
		
		this.currentProject.refreshTasks();
		
		
	}
	
	
	
	
	
}
