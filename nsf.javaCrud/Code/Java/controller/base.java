package controller;

import java.io.Serializable;
import java.util.Map;

import com.ibm.xsp.designer.context.XSPContext;
import com.ibm.xsp.extlib.util.ExtLibUtil;

public class base implements Serializable {

	
	public void print(String msg) {
		
		System.out.println(msg);
		
		
		
	}
	
	
	@SuppressWarnings("unchecked")
	public static String getParam(String key) {
		
		
		// Using the Extention Library Latest Version
		Map<String, String> myMap = (Map<String, String>) ExtLibUtil.resolveVariable("param");
		
		if (myMap.containsKey(key)) {
			return myMap.get(key);
		} else {
			return "";
		}
		
		
		
	}

	
}
