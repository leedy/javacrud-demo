package com.notesin9.tracker;

import java.io.Serializable;
import java.util.Date;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.Session;
import org.openntf.domino.View;
import org.openntf.domino.utils.Factory;

public class Task implements Serializable {

	private static final long serialVersionUID = 1L;

	private String unique;
	private String parentId;
	private String description;
	private String priority;
	private Date dueDate;
	private String notes;
	private String creator;
	private String unid;
	private String assignedTo;

	private boolean valid;

	public Task() {
		this.unid = "";
	}
	
	public void create(Project project) {
		
		Session session = Factory.getSession();  // this will be slightly different if not using the OpenNTF Domino API
		this.setUnique(session.getUnique());
		this.setCreator(session.getEffectiveUserName());
		
		this.parentId = project.getUnique();
		
		this.valid = true;
		

	}
	
	
	public boolean load(String key) {
		
		// this key is the unique key of the document.  UNID would be faster/easier..  I just kinda hate using them and seeing them in URLS
		Session session = Factory.getSession(); 
		Database db = session.getCurrentDatabase();
		View view = db.getView("lkup_taskByKey");
		Document doc = view.getDocumentByKey(key); // This is deprecated because the API prefers to use getFirstDocumentByKey
		
		if (null == doc) {
			// document not found.  DANGER
			this.valid = false;
		} else {
			
			this.loadValues(doc);
			
			
		}
		
		return this.valid;
		
		
	}
	
	public void load(Document doc) {
		
		this.loadValues(doc);
		
		
	}
	
	protected void loadValues(Document doc) {
		
		this.unid = doc.getUniversalID();
		this.unique = doc.getItemValueString("unique");
		this.parentId = doc.getItemValueString("parentId");
		this.description = doc.getItemValueString("description");
		this.priority = doc.getItemValueString("priority");
		this.dueDate = doc.getItemValue("dueDate", Date.class);  // This is API Specific
		this.notes = doc.getItemValueString("notes");
		this.creator = doc.getItemValueString("creator");
		
		this.valid = true;
		
		
		
		
	}
	
	public boolean save() {
		// Just deal with the document here.
		Session session = Factory.getSession(); 
		Database db = session.getCurrentDatabase();
		
		if (this.unid.isEmpty()) {
			// this is a new Doc
			Document doc = db.createDocument();
			doc.replaceItemValue("form", "task");
			this.unid = doc.getUniversalID();
			this.saveValues(doc);
			return doc.save();
			
			
		} else {
			// This document already exists
		
			Document doc = db.getDocumentByUNID(this.getUnid());
			this.saveValues(doc);
			return doc.save();
			
		}
		
		
	}
	
	private void saveValues(Document lookupDoc) {
		
		lookupDoc.replaceItemValue("unique", this.getUnique());
		lookupDoc.replaceItemValue("parentId", this.getParentId());
		lookupDoc.replaceItemValue("description", this.getDescription());
		lookupDoc.replaceItemValue("priority", this.getPriority());
		lookupDoc.replaceItemValue("dueDate", this.getDueDate());  // might be different without the API
		lookupDoc.replaceItemValue("notes", this.getNotes());
		lookupDoc.replaceItemValue("creator", this.getCreator());
		
		lookupDoc.replaceItemValue("assignedTo", this.getAssignedTo());
		
	}
	
	

	public String getUnique() {
		return unique;
	}

	public void setUnique(String unique) {
		this.unique = unique;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getUnid() {
		return unid;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	
	public boolean remove() {
		Session session = Factory.getSession(); 
		Database db = session.getCurrentDatabase();
		Document doc = db.getDocumentByUNID(this.getUnid());
		return doc.remove(true);
		
	}
	

}
