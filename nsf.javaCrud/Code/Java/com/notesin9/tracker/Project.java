package com.notesin9.tracker;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openntf.domino.Database;
import org.openntf.domino.Document;
import org.openntf.domino.DocumentCollection;
import org.openntf.domino.Name;
import org.openntf.domino.Session;
import org.openntf.domino.View;
import org.openntf.domino.utils.Factory;

public class Project implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private String unique;
	private String description;
	private String priority;
	private Date dueDate;
	private String notes;
	private String creator;
	private String unid;
	
	private boolean valid;
	
	private List<Task> allTasks;
	
	
	
	public Project()  {
		
		this.unid = "";
		
	}
	
	public void create() {
		
		Session session = Factory.getSession();  // this will be slightly different if not using the OpenNTF Domino API
		this.setUnique(session.getUnique());
		this.setCreator(session.getEffectiveUserName());
		this.valid = true;
		

	}
	
	public boolean load(String key) {
		
		// this key is the unique key of the document.  UNID would be faster/easier..  I just kinda hate using them and seeing them in URLS
		Session session = Factory.getSession(); 
		Database db = session.getCurrentDatabase();
		View view = db.getView("lkup_project");
		Document doc = view.getDocumentByKey(key); // This is deprecated because the API prefers to use getFirstDocumentByKey
		
		if (null == doc) {
			// document not found.  DANGER
			this.valid = false;
		} else {
			
			this.loadValues(doc);
			
			
		}
		
		return this.valid;
		
		
	}
	
	protected void loadValues(Document doc) {
		
		this.unid = doc.getUniversalID();
		this.unique = doc.getItemValueString("unique");
		this.description = doc.getItemValueString("description");
		this.priority = doc.getItemValueString("priority");
		this.dueDate = doc.getItemValue("dueDate", Date.class);  // This is API Specific
		this.notes = doc.getItemValueString("notes");
		this.creator = doc.getItemValueString("creator");
		
		this.valid = true;
		
		
		
		
	}
	
	

	public boolean save() {
		// Just deal with the document here.
		Session session = Factory.getSession(); 
		Database db = session.getCurrentDatabase();
		
		if (this.unid.isEmpty()) {
			// this is a new Doc
			Document doc = db.createDocument();
			doc.replaceItemValue("form", "project");
			this.unid = doc.getUniversalID();
			this.saveValues(doc);
			return doc.save();
			
			
		} else {
			// This document already exists
		
			Document doc = db.getDocumentByUNID(this.getUnid());
			this.saveValues(doc);
			return doc.save();
			
		}
		
		
	}
	
	private void saveValues(Document lookupDoc) {
		
		lookupDoc.replaceItemValue("unique", this.getUnique());
		lookupDoc.replaceItemValue("description", this.getDescription());
		lookupDoc.replaceItemValue("priority", this.getPriority());
		lookupDoc.replaceItemValue("dueDate", this.getDueDate());  // might be different without the API
		lookupDoc.replaceItemValue("notes", this.getNotes());
		lookupDoc.replaceItemValue("creator", this.getCreator());
		
	}
	


	public String getUnique() {
		return unique;
	}



	public void setUnique(String unique) {
		this.unique = unique;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getPriority() {
		return priority;
	}



	public void setPriority(String priority) {
		this.priority = priority;
	}


	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public String getNotes() {
		return notes;
	}



	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	public String getCreatorCommonName() {
		
		Session session = Factory.getSession();
		Name temp = session.createName(this.getCreator());
		return temp.getCommon();
	}

	public String getUnid() {
		return unid;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	public boolean remove() {
		
		
		
		Session session = Factory.getSession(); 
		Database db = session.getCurrentDatabase();
		
		if (this.unid.isEmpty()) {
			// this is a new Doc
			return false;
			
		} else {
			// This document already exists
		
			// First remove any tasks
			
			// First Delete the Tasks
			
			for (Task task : this.getAllTasks()) {
				task.remove();
			}
			
			
			
			Document doc = db.getDocumentByUNID(this.getUnid());
			return doc.remove(true);
			
		}
		
	}
	
	public Task createTask() {
		
		Task temp = new Task();
		temp.create(this);
		
		
		return temp;
	}

	public List<Task> getAllTasks() {
		
		if (this.allTasks == null) {
			this.allTasks = new ArrayList<Task>();
			
			Session session = Factory.getSession(); 
			Database db = session.getCurrentDatabase();
			View view = db.getView("lkup_tasksByParent");
			DocumentCollection collect = view.getAllDocumentsByKey(this.getUnique());
			
			if (collect == null) {
				return this.allTasks;
			}
			
			for (Document doc : collect) {
				// This loop if specific to the API.  If not using the API
				// then use a while loop.  And you also need to recycle your domino objects.
				
				Task temp = new Task();
				temp.load(doc);
				
				this.allTasks.add(temp);
				
			}
			
			
			
		} else {
			// It's already loaded.  nothing to do.
			
		}
		
	
		
		return allTasks;
	}
	
	public void refreshTasks() {
		
		this.allTasks = null;
		
	}

	
	
	
	
	
	
	
	
}
